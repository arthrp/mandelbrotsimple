﻿namespace MandelbrotSimple;

public static class Program
{
    public static void Main()
    {
        var data = new MandelbrotProviderCustom().GetData(-0.5, 0, 1024);
        var w = new ImageWriter();
        w.WritePpm("test.ppm",1024,1024,data);
    }
}