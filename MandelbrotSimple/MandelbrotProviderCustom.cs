namespace MandelbrotSimple;

/// <summary>
/// Generate the same greyscale image as MandelbrotProviderSimple but define Complex numbers yourself for additional clarity (yes, it's slower and unoptimized)
/// </summary>
public class MandelbrotProviderCustom
{
    public byte[] GetData(double panX, double panY, int sideSize)
    {
        var maxIters = 255;
        double zoomCoef = 3;
        var result = new byte[sideSize*sideSize*3];
        var i = 0;
        
        for (var y = 0; y < sideSize; y++)
        {
            for (var x = 0; x < sideSize; x++)
            {
                var x0 = panX - zoomCoef/2 + zoomCoef*x/sideSize;
                var y0 = panY - zoomCoef/2 + zoomCoef*y/sideSize;

                var c = new ComplexNum(x0, y0);
                var color = maxIters - GetIterations(c, maxIters); // 0 - 255
                result[i] = result[i+1] = result[i+2] = (byte)color;
                i += 3;
            }
        }
        
        return result;
    }
    
    private int GetIterations(ComplexNum c0, int maxIters)
    {
        var z = c0;
        for (int i = 0; i < maxIters; i++)
        {
            if (z.Abs() > 2.0) return i;
            z = c0.Add(z.Multiply(z)); // z = z*z + c
        }

        return maxIters;
    }
}

public record ComplexNum
{
    public ComplexNum(double real, double imaginary)
    {
        Imaginary = imaginary;
        Real = real;
    }

    private double Real { get; }
    private double Imaginary { get; }

    public double Abs()
    {
        return Math.Sqrt(Real * Real + Imaginary * Imaginary);
    }

    public ComplexNum Add(ComplexNum n)
    {
        var real = Real + n.Real;
        var imaginary = Imaginary + n.Imaginary;

        return new ComplexNum(real, imaginary);
    }

    public ComplexNum Multiply(ComplexNum n)
    {
        var real = Real * n.Real - Imaginary * n.Imaginary;
        var img = Real * n.Imaginary + Imaginary * n.Real;

        return new ComplexNum(real, img);
    }
}