using System.Numerics;

namespace MandelbrotSimple;

/// <summary>
/// Generating greyscale Mandelbrot set using Complex numbers for clarity
/// </summary>
public class MandelbrotProviderSimple
{
    public byte[] GetData(double panX, double panY, int sideSize)
    {
        var maxIters = 255;
        double zoomCoef = 3;
        var result = new byte[sideSize*sideSize*3];
        var i = 0;
        
        for (var y = 0; y < sideSize; y++)
        {
            for (var x = 0; x < sideSize; x++)
            {
                var x0 = panX - zoomCoef/2 + zoomCoef*x/sideSize;
                var y0 = panY - zoomCoef/2 + zoomCoef*y/sideSize;

                var c = new Complex(x0, y0);
                var color = maxIters - GetIterations(c, maxIters); // 0 - 255
                result[i] = result[i+1] = result[i+2] = (byte)color;
                i += 3;
            }
        }
        
        return result;
    }

    private int GetIterations(Complex c0, int maxIters)
    {
        var z = c0;
        for (int i = 0; i < maxIters; i++)
        {
            if (Complex.Abs(z) > 2.0) return i;
            z = Complex.Add(Complex.Multiply(z, z), c0); // z = z*z + c
        }

        return maxIters;
    }
}