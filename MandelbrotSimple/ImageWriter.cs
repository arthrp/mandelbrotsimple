namespace MandelbrotSimple;

public class ImageWriter
{
    public void WritePpm(string fileName, int width, int height, byte[] data)
    {
        using (var writer = new StreamWriter(fileName))
        {
            writer.WriteLine("P6");
            writer.WriteLine($"{width} {height}");
            writer.WriteLine("255");
            writer.Close();
        }

        using (var binaryWriter = new FileStream(fileName, FileMode.Append))
        {
            binaryWriter.Write(data);
            binaryWriter.Close();
        }
    }
}